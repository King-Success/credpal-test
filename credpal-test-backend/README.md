# Credpal_test

Developer test for credpal.com

## Endpoints

- POST api/books
- GET  api/books
- POST api/books/{id}/reviews
- POST api/login
- POST api/register       

## Admin login

- email: admin@gmail.com
- password: password

## Sample requests

The fallowing are sample request parameters:

####POST api/books

**Json object**
```
{
   	"isbn": "1234567982765",
   	"title": "Book Title",
   	"description": "Book description",
   	"authors": [1,2]
}
```

**Request headers**
```
{
   "Content-Type": "application/json",
   "Accept": "application/json",
   "Authorization": "Bearer $access_token",
}
```
**Requirements**

- Must be authenticated
- Must be signed in as a admin

#### GET  api/books
**Request params**

- page: page number.
- sortColumn: one of `title` or `avg_review`.
- sortDirection: one of `ASC` or `DESC`.
- title: search by book title.
- authors: search by author’s ID (comma-separated).

**Request headers**
```
{
   "Content-Type": "application/json",
   "Accept": "application/json",
}
```
**Requirements**

- None

#### POST api/books/{id}/reviews

**Json object**
```
{
	"review": 5,
    "comment": "This was an average read, nothing too special"
}
```
**Request headers**
```
{
   "Content-Type": "application/json",
   "Accept": "application/json",
   "Authorization": "Bearer $access_token",
}
```

**Requirements**

- Must be authenticated

#### POST api/login

**Json object**

```
{
	"email": "admin@gmail.com",
	"password": "password"
}
``` 
**Request headers**

```
{
   "Content-Type": "application/json",
   "Accept": "application/json",
}
```

**Requirements**

- None

#### POST api/register
**Json object**

```
{
	"name": "Lorem",
	"surname": "Ipsum",
	"email": "hello@gmail.com",
	"password": "secret",
	"password_confirmation": "secret"
}
```
**Request headers**

```
{
   "Content-Type": "application/json",
   "Accept": "application/json",
}
```
**Requirements**

- None

## Installation

- Clone this repository.
- Run `composer install` to install all dependencies.
- Modify .env file, add database connection params.
- Run `php artisan migrate`.
- Run `php artisan db:seed`.
- Run `php artisan passport:install`.

You should be done with the basic installation and configuration.


