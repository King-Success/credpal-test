<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    public function authors (){
        return $this->belongsToMany('App\User', 'author_book', 'author_id', 'book_id');
    }

    public function reviews (){
        return $this->hasMany('App\Review');
    }
}
