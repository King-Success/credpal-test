<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Laravel\Passport\Exceptions\MissingScopeException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response
     * @throws Exception
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            if ($request->is('api/*')) {
                return response()->json(['error' => 'Model Not Found'], 404);
            }
            return response()->view('404', [], 404);
        }elseif($exception instanceof MissingScopeException){
            if ($request->is('api/*')) {
                return response()->json(['error' => 'Your role does not permit this action'], 401);
            }
            return response()->view('401', [], 401);
        }
        return parent::render($request, $exception);
    }
}
