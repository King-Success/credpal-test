<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Review as ReviewResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'isbn' => $this->isbn,
            'title' =>$this->title,
            'description' => $this->description,
            'authors' => UserResource::collection($this->authors),
            'review' => [
                'avg' => $this->reviews()->avg('review'),
                'count' => $this->reviews()->count()
            ],
        ];
    }
}
