<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\User as UserResource;

class ApiAuthController extends Controller
{

    /**
     * registers a newly created user.
     *
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed'
        ]);
        $validatedData['password'] = bcrypt($request->input('password'));
        $user = User::create($validatedData);

        $accessToken = $user->createToken('apiAuthToken', ['create-reviews'])->accessToken;

        return response()->json(['user' => new UserResource($user), 'access_token' => $accessToken]);

    }

    /**
     * logs in a user.
     *
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email',
            'password' => 'required'
        ]);

        if(!auth()->attempt($loginData)){
            return response()->json(['message' => 'Invalid Credentials'])->setStatusCode(403);
        }
        if(auth()->user()->isAdmin){
            $accessToken = auth()->user()->createToken('apiAuthToken', ['create-books', 'create-reviews'])->accessToken;
        }else {
            $accessToken = auth()->user()->createToken('apiAuthToken', ['create-reviews'])->accessToken;
        }

        return response()->json(['user' => new UserResource(auth()->user()), 'access_token' => $accessToken]);
    }

}
