<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Http\Resources\Book as BookResource;
use App\Http\Resources\Review as ReviewResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use function foo\func;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request .
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
            $title = $request->get('title');
            $perPage = $request->get('per_page') != '' ? $request->get('per_page') : 5;
            $sortColumn = $request->get('sortColumn') != '' ? $request->get('sortColumn') : 'id';
            $sortDirection = $request->get('sortDirection') != '' ? $request->get('sortDirection') : 'asc';
            if($sortColumn == 'avg_review'){
                $sortColumn = 'reviews_count';
            }
            $withPath = 'http://localhost:8000/api/books' . '?sortColumn=' . $sortColumn . '&sortDirection=' . $sortDirection . '&per_page' . $perPage;
            $withPath = $title != '' ? $withPath . '&title' . $title : $withPath;
            $books = Book::where('title', 'like', '%' . $title . '%')
                ->with(['authors', 'reviews'])
                ->orderBy($sortColumn, $sortDirection)
                ->paginate($perPage)
                ->withPath($withPath);
        return BookResource::collection($books);
    }

    /**
     * Stores a new instance.
     *
     * @param Request $request
     * @return BookResource
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'isbn' => 'required|digits:13|unique:books,isbn',
                'title' => 'required|string',
                'description' => 'required|string',
                'authors' => 'required|array',
                'authors.*' => 'integer|exists:users,id'
            ]
        );
        $book = new Book;
        $book->isbn = $request->input('isbn');
        $book->title = $request->input('title');
        $book->description = $request->input('description');

        if($book->save()){
            $book->authors()->attach($request->input('authors'));
            return new BookResource($book);
        }
    }

    /**
     * Stores a new review instance.
     *
     * @param Request $request
     * @param int $id
     * @return ReviewResource
     */
    public function storeReview(Request $request, $id)
    {
        $request->validate([
            'review' => 'required|digits_between:1,10',
            'comment' => 'required|string'
        ]);

        $book = Book::findOrFail($id);
        $review = $book->reviews()->create([
            'review' => $request->input('review'),
            'comment' => $request->input('comment'),
            'user_id' => auth()->user()->id
        ]);
        return new ReviewResource($review);
    }

}
