<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Book::class, 50)->create()->each(function($book){
            $book->reviews()->createMany(factory(App\Review::class, rand(1,5))->make()->toArray());
        });
    }
}
