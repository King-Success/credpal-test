<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BooksTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $authors = App\User::all();
         App\Book::all()->each(function($book) use($authors){
             $book->authors()->attach(
                 $authors->random(rand(1, 3))->pluck('id')->toArray()
             );
         });
    }
}
