<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create an admin user first
        App\User::create([
            "name" => "Kingsley",
            "surname" => "Arinze",
            "email" => "admin@gmail.com",
            'email_verified_at' => now(),
            'isAdmin' => true,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        factory(App\User::class, 3)->create();
    }
}
