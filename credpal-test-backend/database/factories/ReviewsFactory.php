<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'book_id' => $faker->numberBetween(1, 10),
        'user_id' => $faker->numberBetween(1, 3),
        'review' => $faker->numberBetween(1, 10),
        'comment' => $faker->sentence(15, true)
    ];
});
