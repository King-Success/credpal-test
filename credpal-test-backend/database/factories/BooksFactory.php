<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
            'title' => $faker->sentence(5, true),
            'isbn' => $faker->isbn13(),
            'description' => $faker->text()
    ];
});
