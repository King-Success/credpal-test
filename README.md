# credpal-test

Credpal developer test built with Laravel and vue.js

## Project setup

Clone this repository `https://gitlab.com/King-Success/credpal-test.git`. This repo contains both the backend and the frontend applications.

On the root, `cd credpal-test-backend` and `cd credpal-test-frontend` on different bash/shell windows.

#### Setup Frontend

- Run `yarn install or npm install` to install all dependencies.
- Run `yarn run serve or yarn run serve` to start dev server.
  Ensure you have you vue.js enviroment setup. See official documentation for details.

#### Setup Backend

- Run `composer install` to install all dependencies.
- Modify .env file, add database params.
- Run `php artisan migrate`.
- Run `php artisan db:seed`.
- Run `php artisan passport:install`.
  Ensure you have your Laravel enviroment setup, See official documentation for details.
