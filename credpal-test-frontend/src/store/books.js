import axios from "axios";

export default {
  namespaced: true,

  state: {
    allBooks: null,
    links: null,
    meta: null
  },

  getters: {
    books(state) {
      return state.allBooks;
    },
    links(state) {
      return state.links;
    },
    meta(state) {
      return state.meta;
    }
  },

  mutations: {
    SET_BOOKS(state, payload) {
      state.allBooks = payload.data.map((book) => {
        return {
          id: book.id,
          isbn: book.isbn,
          title: book.title,
          description: book.description,
          authors: book.authors
            .map((author) => `${author.name} ${author.surname}`)
            .join(", "),
          review: Math.round(book.review.avg * 100) / 100
        };
      });
      state.links = payload.links;
      state.meta = payload.meta;
    }
  },

  actions: {
    async getBooks({ commit, state }, url = "/books?per_page=50") {
      let response = await axios.get(url);
      if (response.data) {
        commit("SET_BOOKS", response.data);
      }

      if (!state.allBooks) {
        return;
      }
    }
  }
};
