import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth";
import books from "./books";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    //
  },

  mutations: {
    //
  },

  actions: {
    //
  },

  modules: {
    auth,
    books
  }
});
