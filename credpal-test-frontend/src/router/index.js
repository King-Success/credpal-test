import Vue from "vue";
import VueRouter from "vue-router";
import SignIn from "../views/SignIn.vue";
import SignUp from "../views/SignUp.vue";
import Books from "../views/Books.vue";
import store from "../store";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: SignIn
  },
  {
    path: "/signin",
    name: "signin",
    component: SignIn
  },
  {
    path: "/signup",
    name: "signup",
    component: SignUp
  },
  {
    path: "/books",
    name: "books",
    component: Books,
    beforeEnter(to, from, next) {
      try {
        const hasPermission = store.getters["auth/authenticated"];
        if (hasPermission) {
          next();
        } else {
          next({
            name: "signin"
          });
        }
      } catch (e) {
        next({
          name: "signin"
        });
      }
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
